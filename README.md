#Quick Weight Loss Center
_____________________________________________________________

##The SunGlass Club (Landing Page) 
1. Clone this project using GIT.
2. To edit please, setup your local environment.
*[browserSync](https://browsersync.io/ "browserSync"):*
   Setup [MAMP](https://www.mamp.info/en/ "MAMP") or [WAMP](http://www.wampserver.com/en/ "MAMP")  - Edit your host file
   Edit line 18 from /gulpfile.js
*No BrowserSync: *
   Uncomment line 149 - delete line 148
3. Install NPM [installed] (https://www.npmjs.com/get-npm) & run Gulp. 		
`$ npm install `
`$ gulp `


4. Edit CSS: /sass - JS: /js/

##Tell us about an important technical decision you had to make on a recent project.

###What was the situation?

A client with a presence in the entire country had an old website [old website](https://web.archive.org/web/20171115161627/https://www.florajen.com/) without CMS, not mobile friendly, not SEO best practices. They had a convention, so they a modern site in a short deadline and they want a fully customizable website, with the ability to provide support to the Health Care Profesional.


###How did you understand the problem?

Since I have substantial experience developing websites & e-commerce fully responsive, SEO friendly, I knew exactly how to go ahead and solve the problem.
 

###What were your options? 

The client also wanted an e-commerce platform in the future, but at the same time needs the website ASAP.
1. Develop the site using WordPress.
2. Develop the site using Magento 2 / Prestashop / Shopify.
3. Develop the site using a custom CMS.

###What informed your final decision? 

Client, time of delivery, Web development best practices.


###Who else was involved? 

Client, UI/EX Web Designer, Account Executive.

###What was the outcome? 

I developed the website using the design provided from the UI/EX web designer in WordPress, creating a custom theme with support to WooCommerce and custom functions to handle Health Care Portal, fully responsive site and SEO friendly, updating all Crawled URLs by search engines to the new URLs.

###You may create diagrams but please write in narrative form: we want an essay, not a presentation.

I was hired as the web developer to be part of the new development to the new website for Florajen.com.
Florajen is a probiotic brand who has a presence in the entire country, the problem eradicated on their website an unupdated website with main issues: Bad UI/UX, no CMS, no mobile friendly.

Since they had a new big convention, they want to have a professional website to share the product information for general public and health professional.

After the kick out meeting, we had a complete idea what the client wants, so we start working.

I worked closely with the UI/EX Web Designer to deliver a perfect design to our client, after the final review the client agreed with the new design.

I had the green light to start development; the UI/EX Web Designer provide me the design in Sketch files.

I started creating a git repository on my local environment and Bitbucket, then I processed to set up my localhost environment, in this case, I used MAMP, modified my host file to run locally correctly.
Then downloaded the latest version of WordPress(WP), create a new DB using command lines, and setup WP using WP-CLI, I created the new theme and with the help of NPM installed gulp for automatization for SASS, JS. 
I started developing the theme using the best practices; also a fully functions script to have the Health Care Professionals Portal working correctly.

Push to a dev environment, fix bugs & update content as the client requested, then approved by a client.

Push to production using GIT, Proper Website Quality Assurance, create 301 permanent redirects, update sitemap, submit to google/bing webmaster tools.

I had a happy client, a new modern website! and this process took us less than four weeks (2 development - 1 design - 1 updating content on through the CMS  )

Please visit [Florajen](http://www.Florajen.com/)


##Using CSS, HTML, and JavaScript, create a landing page to promote a bundled packaged good product that is sold as a subscription (think Dollar Shave Club).


###My write-up:

####The SunGlass Club

Since I notice that a lot of people love sunglasses and I have direct contact from a distributor, I thought that would be amazing to have bundle subscription to get new shades.

The business model is super simple you select your plan:
SILVER 		- 1 Sunglasses per month
GOLD			- 3 Sunglasses per month
Platinum 	- 6 Sunglasses per month

For this particular landing page, I'm going to use Bootstrap 4, HTML, JS, SCSS, and GULP.

To work on some graphics, I'm going to use Sketch. 

The landing will have the options, also a questions CTA.

In the future, to complete the process must be as simple as two clicks:

1. CTA Subscription.
2. Add Basic info: email, name, gender.

Then I will capture the information:

1. If the user wants to continue the process, they need to complete their profile and of course, add their payment information to proceed the subscription. 

2. If the user doesn't continue, I will send him a reminder by email optimized by A/B testing.

